//No. 1 -Mengubah Fungsi Menjadi Arrow
var golden = () => {
    console.log("this is golden!!")
}
golden()
console.log("")

//No. 2 -Sederhanakan Menjadi Object Literal di ES6
newFunction = (firstName, lastName) => {
    firstName
    lastName
    return{
        fullName(){
            console.log(firstName + " " + lastName)
        }
    }
}
//Drive Code
newFunction("William", "Imoh").fullName()
console.log("")

//No. 3 -Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation, spell)
console.log("")

//No. 4 -Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [...west, ...east]
//Drive Code
console.log(combinedArray)
console.log("")

//No. 5 -Template Literals
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} do eiusmod tempor
incididunt ut labore et dolore magna aliqua. ut enim ad minim veniam`
//Drive Code
console.log(before);
console.log("")