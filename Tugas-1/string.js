var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';
console.log(word,second,third,fourth,fifth,sixth,seventh);

var sentence = "I am going to be React Native Developer";
var FirstWord = sentence[0];
var SecondWord = sentence[2] + sentence[3];
var ThirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var FourthWord = sentence[11] + sentence[12];
var FifthWord = sentence[14] + sentence[15];
var SixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var SeventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var EighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];
console.log('First Word: ' + FirstWord);
console.log('Second Word: ' + SecondWord);
console.log('Third Word: ' + ThirdWord);
console.log('Fourth Word: ' + FourthWord);
console.log('Fifth Word: ' + FifthWord);
console.log('Sixth Word: ' + SixthWord);
console.log('Seventh Word: ' + SeventhWord);
console.log('Eighth Word: ' + EighthWord);

var sentence2 = 'wow JavaScript is so cool';
var FirstWord2 = sentence2.substring(0, 3);
var SecondWord2 = sentence2.substring(4, 14);
var ThirdWord2 = sentence2.substring(15, 17);
var FourthWord2 = sentence2.substring(18, 20);
var FifthWord2 = sentence2.substring(21, 25);
console.log('First Word: ' + FirstWord2);
console.log('Second Word: ' + SecondWord2);
console.log('Third Word: ' + ThirdWord2);
console.log('Fourth Word: ' + FourthWord2);
console.log('Fifth Word: ' + FifthWord2);

var sentence3 = 'wow Javascript is so cool';
var FirstWord3 = sentence3.substring(0, 3);
var SecondWord3 = sentence3.substring(4, 14);
var ThirdWord3 = sentence3.substring(15, 17);
var FourthWord3 = sentence3.substring(18, 20);
var FifthWord3 = sentence3.substring(21, 25);
var FirstWordLength = FirstWord3.length
var SecondWordLength = SecondWord3.length
var ThirdWordLength = ThirdWord3.length
var FourthWordLength = FourthWord3.length
var FifthWordLength = FifthWord3.length
console.log('First Word: ' + FirstWord3 + ', with length: ' + FirstWordLength);
console.log('Second Word: ' + SecondWord3 + ', with length: ' + SecondWordLength);
console.log('Third Word: ' + ThirdWord3 + ', with length: ' + ThirdWordLength);
console.log('Fourth Word: ' + FourthWord3 + ', with length: ' + FourthWordLength);
console.log('Fifth Word: ' + FifthWord3 + ', with length: ' + FifthWordLength);
