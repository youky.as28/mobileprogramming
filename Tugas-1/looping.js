//LOOPING WHILE
console.log('Looping Pertama')
var loop = 2;
while(loop < 20){
    if(loop %2==0){
        console.log(loop + '- I LOVE CODING');
    }
    loop++
}

console.log('\n');
console.log('Looping Kedua')
while(loop>=2){
    if(loop %2==0){
        console.log(loop + '- I Will become fullstack javacript developer');
    }
    loop--
}

console.log('\n');
//LOOPING FOR
for (var i=1;i<=20;i++){
    if(i%2==0){
        console.log(i+ "- Informatika")
    }else{
        console.log(i+ "- Teknik")
        console.log(i+ "- I LOVE CODING")
    }
}

//MEMBUAT PERSEGI PANJANG # 
var a=' ';

for(var i = 0; i<4; i++){
    for(var j=0; j<8; j++){
        a +='#';
    }
    a +='\n';
}
console.log(a);

//MEMBUAT TANGGA
var b=' ';

for(i=1; i<=7; i++){
    for(j=1; j<=i; j++){
        b += '#';
    }
    b += '\n';
}
console.log(b);

//MEMBUAT PAPAN CATUR
for(var angka=1; angka<=4; angka++){
    console.log('# # # # #');
    console.log(' # # # # #');
}